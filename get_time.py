import pandas as pd
import json
from pandas.io.json import json_normalize
import numpy as np
import matplotlib.pyplot as plt
from geopy.distance import distance
from matplotlib import cm
import argparse


def build_db(code_postal):
    trajets_taf = pd.read_csv(
        'base-texte-flux-mobilite-domicile-lieu-travail-2016_utf8.txt',
        sep=";", header=0, dtype=str)
    with open('laposte_hexasmal.json') as fp:
        laposte_nested = json.load(fp)
    laposte = json_normalize(laposte_nested)

    # Look for INSEE code
    laposte_data = laposte.loc[laposte['fields.code_postal'] == code_postal]
    code_insee = laposte_data.iloc[0]['fields.code_commune_insee']
    ville = laposte_data.iloc[0]['fields.nom_de_la_commune']

    work_or_live_here = trajets_taf.loc[(trajets_taf['CODGEO'] == code_insee) |
                                        (trajets_taf['DCLT'] == code_insee)
                                        ]
    work_or_live_here = pd.merge(work_or_live_here, laposte,
                                 left_on='CODGEO',
                                 right_on='fields.code_commune_insee'
                                 )
    work_or_live_here = pd.merge(work_or_live_here, laposte,
                                 left_on='DCLT',
                                 right_on='fields.code_commune_insee',
                                 suffixes=('_home', '_work')
                                 )

    d_taf = np.zeros((len(work_or_live_here)))
    for index, row in work_or_live_here.iterrows():
        try:
            d_taf[index] = distance(row['fields.coordonnees_gps_home'],
                                    row['fields.coordonnees_gps_work']).km
        except ValueError:
            print("Error on distance on row:\n{}".format(row))

    work_or_live_here["d_taf"] = pd.Series(d_taf,
                                           index=work_or_live_here.index)
    return ville, work_or_live_here


def gen_pie_chart(db, mean_speed_km_h, ville):
    binning = 10  # minutes
    n_bins = 6

    cs = cm.Spectral(1 - np.arange(n_bins + 1) / (n_bins + 1))

    histo = np.zeros(n_bins + 1)

    for index, row in db.iterrows():
        t = 60 * float(row['d_taf']) / mean_speed_km_h
        bin_num = int(np.floor(t / binning))
        histo[min(n_bins, bin_num)] += float(row['NBFLUX_C16_ACTOCC15P'])

    sizes = 100 * histo / histo.sum()

    labels = []
    for n in range(1, n_bins + 1):
        labels.append("< {} mn".format(binning * n))
    labels.append("> {} mn".format(n_bins * binning))

    fig, ax = plt.subplots(dpi=150)
    ax.pie(sizes, labels=labels, autopct='%1.1f%%', shadow=False,
           startangle=90, colors=cs, counterclock=False)
    ax.axis('equal')

    ax.set_title(
        ("Temps de parcours en provenance et à destination de\n"
         "{}, vitesse {} km/h").format(ville, mean_speed_km_h))

    fig.savefig("diag.png")
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "code_postal", type=int,
        help="code postal de destination et de provenance")
    parser.add_argument(
        "-v", "--vitesse", type=int, default=15,
        help="Vitesse moyenne en km/h (défaut : 15)")

    args = parser.parse_args()

    ville, db = build_db(str(args.code_postal))
    gen_pie_chart(db, args.vitesse, ville)
